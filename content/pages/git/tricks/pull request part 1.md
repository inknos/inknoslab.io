---
title: git/tricks/pull request part 1
tags:
categories:
date: 2023-08-23
lastMod: 2023-08-23
---
to clone a branch[^git-pr-alias]
```bash
git push git@github.com:owner/repo.git HEAD:target-branch
```
In summary: You can push to an existing pull request if you push to the fork/branch that PR is based on. It is often possible depending on repo settings.

```bash
git push git@github.com:username/repo-name.git localbranchname:remotebranchname
```

or if you have the fork added as a remote in your local repo, then:

```bash
git push remotename localbranchname:remotebranchname
```

[^git-pr-alias]: #[git/tricks/useful aliases part 1]({{< ref "/pages/git/tricks/useful aliases part 1" >}}) 
[^stack]: https://stackoverflow.com/questions/15530510/how-do-i-push-to-a-pull-request-on-github
