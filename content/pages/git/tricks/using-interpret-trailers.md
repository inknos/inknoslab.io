---
title: git/tricks/using-interpret-trailers
tags:
categories:
date: 2023-08-15
lastMod: 2023-08-15
---
If you are using intepret-trailers in a git commit such as this:

```bash
Commit Title

My awesome multiline
commit message

Reviewed-by: John Doe
Signed-off-by: John Doe <jdoe@jdoe.net>
```



```bash
$ cat >.git/hooks/commit-msg <<EOF
> #!/bin/sh
> git interpret-trailers --trim-empty --trailer "git-version: \$(git describe)" "\$1" > "\$1.new"
> mv "\$1.new" "\$1"
> EOF
$ chmod +x .git/hooks/commit-msg
```
